
import 'package:esper/model/skill_list_data.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/view/widgets/ESSkillWidgets/es_skill_card.dart';
import 'package:flutter/material.dart';


class SkillTab extends StatefulWidget {
  @override
  State<SkillTab> createState() => _SkillTabState();
}

class _SkillTabState extends State<SkillTab> {
  SkillListData data = SkillListData();

  @override
  Widget build(BuildContext context) {
    return SkillBody(data.skillListData);
  }
}

class SkillBody extends StatefulWidget {
  final List<SkillCardItem> _data;
  SkillBody(this._data);

  @override
  State<SkillBody> createState() => _SkillBodyState();
}

class _SkillBodyState extends State<SkillBody> {


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(EsparImageAssets.bgSkill),
          fit: BoxFit.cover,
        ),
      ),
      padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
      child: _buildPanel(),
    );
  }

  Widget _buildPanel() {
    return ListView.builder(
      itemCount: widget._data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
          child: ESSkillCard(widget._data[index]),
        );
      },
    );
  }
}
