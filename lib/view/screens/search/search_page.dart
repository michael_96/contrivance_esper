import 'dart:math';

import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:esper/utils/libraries/typeahead/src/flutter_typeahead.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _typeAheadController = TextEditingController();
  String _selectedSkill = "";
  Widget _createTips(List<String> tips) {
    TextStyle _style = TextStyle(
      color: EsparColors.textSecondary,
      fontFamily: 'Roboto',
      fontSize: 12,
      fontWeight: FontWeight.normal,
    );
    return SizedBox(
      height: 110,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'TIPs',
            style: _style,
          ),
          const Divider(),
          ListView.builder(
            physics: const ClampingScrollPhysics(),
            shrinkWrap: true,
            itemCount: tips.length,
            itemBuilder: (BuildContext context, int index) {
              return Text(
                tips[index],
                style: _style,
              );
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    String _selectedSkill = "";
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(EsparImageAssets.searchBg),
                fit: BoxFit.cover)),
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'ADD SKILL',
                        style: ESTextStyle.bodyHighlightBig,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: SizedBox(
                              width: 250,
                              child: TypeAheadFormField(
                                suggestionsBoxDecoration:
                                    SuggestionsBoxDecoration(
                                      constraints: BoxConstraints(
                                        maxHeight: 250
                                      ),
                                  elevation: 0,
                                  color: EsparColors.transparent,
                                  shadowColor: EsparColors.transparent,
                                ),
                                textFieldConfiguration: TextFieldConfiguration(
                                  controller: _typeAheadController,
                                  decoration: const InputDecoration(
                                    labelText: 'I am a search box',
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    focusedBorder: UnderlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5.0)),
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                  ),
                                ),
                                suggestionsCallback: (pattern) {
                                  return SkillService.getSuggestions(pattern);
                                },
                                itemBuilder: (context, suggestion) {
                                  return SizedBox(
                                    height: 40,
                                    child: Padding(
                                      padding: const EdgeInsets.all(15),
                                      child: Text(
                                        suggestion.toString(),
                                        style: ESTextStyle.smallText1.merge(
                                          TextStyle(
                                              fontWeight: (suggestion.toString() ==
                                                  "Add New +")
                                                  ? FontWeight.bold
                                                  : FontWeight.normal),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                transitionBuilder:
                                    (context, suggestionsBox, controller) {
                                  return suggestionsBox;
                                },
                                  validator: (value) {
                                    if (value.toString().isEmpty) {
                                      return 'Please select a skill';
                                    }
                                  },
                                onSuggestionSelected: (suggestion) {
                                  (suggestion.toString() == "Add New +")
                                      ? Scaffold.of(context).showSnackBar(SnackBar(content: Text('Going to add new skill page')
                                  )) // TODO 2: - Open add new skill page
                                      : _typeAheadController.text =
                                          suggestion.toString();
                                },
                                onSaved: (value) =>
                                    _selectedSkill = value.toString(),
                              ),
                            ),
                          ),
                          Icon(
                            Icons.search,
                            color: EsparColors.primaryHappyYellow,
                          )
                        ],
                      ),
                      SizedBox(
                        width: 150,
                        height: 50,
                        child: TextButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  EsparColors.transparentThemeDark),
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.white)),
                          onPressed: () {
                            setState(() {
                              // TODO 1: - Goto next page
                              if (_formKey.currentState?.validate() == true) {
                                _formKey.currentState?.save();
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text('Your Selected Skill is $_selectedSkill')
                                ));
                              } else {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text('Please select a skill')
                                ));
                              }
                            });
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: const [
                              Text('Next'),
                              Icon(Icons.arrow_right_alt)
                            ],
                          ),
                        ),
                      ),
                      _createTips([
                        '1. I am an explanatory text. Integer vestibulum fringilla id lobortis ut.',
                        '2.Nibh ullamcorper viverra lectus quis feugiat semper.'
                      ]),
                    ],
                  ),
                ),
              ),
            )
          ],
        ));
  }
}

class SkillService {
  static final List<String> skills = [
    'Teakwondo',
    'Karnatic Music',
    'Bharatnatyam',
    'Teakwondo1',
    'Karnatic Music1',
    'Bharatnatyam1',
    'Teakwondo2',
    'Karnatic Music2',
    'Bharatnatyam2',
    'Teakwondo3',
    'Karnatic Music3',
    'Bharatnatyam3'
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = [];
    matches.addAll(skills);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    matches.add("Add New +");
    return matches;
  }
}
