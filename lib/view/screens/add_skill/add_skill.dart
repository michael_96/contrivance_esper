
import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:esper/view/widgets/es_custom_button.dart';
import 'package:flutter/material.dart';

class AddSkill extends StatefulWidget {
  const AddSkill({Key? key}) : super(key: key);

  @override
  State<AddSkill> createState() => _AddSkillState();
}

class _AddSkillState extends State<AddSkill> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:  BoxDecoration(
        image: DecorationImage(
          image: AssetImage(EsparImageAssets.addBg),
          fit: BoxFit.fill,
        ),
      ),
      child: Column(
        children: [
          Flexible(
            child: CustomScrollView(
              slivers: [
                SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                         Padding(
                          padding: EdgeInsets.only(
                            top: 20,
                            bottom: 20,
                          ),
                          child: Text(
                            'ADD STYLE',
                            style: ESTextStyle.bodyHighlight.merge(TextStyle(fontSize: 22)),
                          ),
                        ),
                         Padding(
                          padding: EdgeInsets.only(
                            bottom: 40,
                          ),
                          child: Text(
                            'BHARATNATYAM',
                            style: ESTextStyle.smallText1,
                          ),
                        ),
                        SizedBox(
                            height: 180,
                            child: TextButton(
                                onPressed: () {
                                  //..
                                },
                                child:
                                    Image.asset(EsparImageAssets.addSkillImage))),
                        Container(
                          padding: const EdgeInsets.all(20),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 0,
                            color: EsparColors.appTabThemeLight,
                            child: Padding(
                              padding: EdgeInsets.all(20),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      TextButton(
                                          onPressed: () {
                                            print('Add button tapped');
                                          },
                                          child: Image.asset(
                                              EsparImageAssets.ic_white_add)),
                                       Text('Others',
                                          style: ESTextStyle.heading1)
                                    ],
                                  ),
                                  Text(
                                      'Integer vestibulum fringilla id lobortis ut. Nibh ullamcorper viverra lectus quis feugiat semper. Velit faucibus sed placerat praesent risus praesent donec. Faucibus viverra elementum tempus, magna mi faucibus est neque. Congue amet, vitae pulvinar dolor congue amet nulla. Dignissim tristique pellentesque quis porta eget. In a tristique quis nulla egestas non felis eget. ',
                                      style: ESTextStyle.mediumCopy),
                                  const SizedBox(
                                    height: 10,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          ),
          ESCustomButton(
              inputText: 'ADD NEW  +',
              width: 150,
              onClickChanged: () {
                print('Added New Skill');
                // setState(() {});
              }),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
