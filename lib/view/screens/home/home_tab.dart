
import 'package:esper/model/select_skill_data.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/view/widgets/es_select_skill.dart';
import 'package:flutter/material.dart';
import '../../widgets/es_custom_button.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  State<HomeTab> createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:  BoxDecoration(
        image: DecorationImage(
          image: AssetImage(EsparImageAssets.bgSkill),
          fit: BoxFit.cover,
        ),
      ),
      child: OnboardView(),
    );
  }
}

class OnboardView extends StatefulWidget {
  const OnboardView({Key? key}) : super(key: key);

  @override
  State<OnboardView> createState() => _OnboardViewState();
}

class _OnboardViewState extends State<OnboardView> {
  SelectListData data = SelectListData();
  var currentPageValue = 0;
  void getChangedPageAndMoveBar(int page) {
    //  currentPageValue = page;
    setState(() {
      currentPageValue = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    final PageController _pageController =
        PageController(initialPage: currentPageValue);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          child: PageView.builder(
            controller: _pageController,
            itemCount: data.skillList.length,
            itemBuilder: (BuildContext context, int index) {
              return ESSelectSkill(data.skillList[index]);
            },
            physics: ClampingScrollPhysics(),
            onPageChanged: (int index) {
              getChangedPageAndMoveBar(index);
            },
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Stack(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  for (int i = 0; i < data.skillList.length; i++)
                    if (i == currentPageValue) ...[circleBar(true)] else
                      circleBar(false),
                ],
              ),
            ),
          ],
        ),
        ESCustomButton(
            inputText: 'SELECT',
            width: 135,
            onClickChanged: () {
              var skillType = data.skillList[currentPageValue].skillType;
              print('You have selected $skillType');
            }),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}

Widget circleBar(bool isActive) {
  return AnimatedContainer(
    duration: Duration(milliseconds: 150),
    margin: EdgeInsets.symmetric(horizontal: 8),
    height: isActive ? 12 : 11,
    width: isActive ? 12 : 11,
    decoration: BoxDecoration(
        color: isActive ? Colors.grey : Colors.transparent,
        border: isActive
            ? Border.all(
                width: 0.0,
              )
            : Border.all(
                width: 1.0,
              ),
        borderRadius: BorderRadius.all(Radius.circular(10))),
  );
}

