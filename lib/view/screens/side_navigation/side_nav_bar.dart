import 'package:esper/model/side_navigation_item.dart';
import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_images.dart';
import 'package:flutter/material.dart';

class SideNavBar {
  List<SideNavigationItem> list = [
    SideNavigationItem(
      title: '',
      leadingIconAsset: const SizedBox(
        height: 25,
      ), isClickable: false,
    ),
    SideNavigationItem(
        title: 'PROFILE',
        leadingIconAsset: Image.asset(EsparMenuAssets.ic_profile),
        trailingIconAsset: SizedBox(
          width: 120,
          child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(EsparColors.appTabThemeDark),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: EsparColors.appTabThemeDark),
                ))),
            onPressed: () {
              //
            },
            child: const Text(
              'Search',
              style: TextStyle(color: Colors.black),
            ),
          ),
        ), isHeader: true),
    SideNavigationItem(
        title: 'Skills', leadingIconAsset: Image.asset(EsparMenuAssets.ic_skill),trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus), ),
    SideNavigationItem(
        title: 'Achievements', leadingIconAsset: Image.asset(EsparMenuAssets.ic_achievements),trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus),),
    SideNavigationItem(
        title: 'Endorsements', leadingIconAsset: Image.asset(EsparMenuAssets.ic_endorsement)),
    SideNavigationItem(
      title: '',
      leadingIconAsset: const SizedBox(
        height: 25,
      ), isClickable: false,
    ),
    SideNavigationItem(
        title: 'Cohorts', leadingIconAsset: Image.asset(EsparMenuAssets.ic_endorsement), isHeader: true),
    SideNavigationItem(
        title: 'Clubs', leadingIconAsset: Image.asset(EsparMenuAssets.ic_club)),
    SideNavigationItem(
        title: 'SIGs', leadingIconAsset: Image.asset(EsparMenuAssets.ic_sig),trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus),),
    SideNavigationItem(
      title: '',
      leadingIconAsset: const SizedBox(
        height: 25,
      ), isClickable: false,
    ),
    SideNavigationItem(
        title: 'Refer Org.', leadingIconAsset: Image.asset(EsparMenuAssets.ic_refer), isHeader: true,trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus),),
    SideNavigationItem(
        title: 'Invite Users', leadingIconAsset: Image.asset(EsparMenuAssets.ic_invite),trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus),),
    SideNavigationItem(
        title: 'Messages', leadingIconAsset: Image.asset(EsparMenuAssets.ic_msg),trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus),),
    SideNavigationItem(
        title: 'Chats', leadingIconAsset: Image.asset(EsparMenuAssets.ic_chat),trailingIconAsset: Image.asset(EsparMenuAssets.ic_plus),),
    SideNavigationItem(
      title: '',
      leadingIconAsset: const SizedBox(
        height: 25,
      ), isClickable: false,
    ),
    SideNavigationItem(
        title: 'Activity Log', leadingIconAsset: Image.asset(EsparMenuAssets.ic_activity)),
    SideNavigationItem(
        title: 'Settings', leadingIconAsset: Image.asset(EsparMenuAssets.ic_settings)),
    SideNavigationItem(
        title: 'Logout', leadingIconAsset: Image.asset(EsparMenuAssets.ic_logout)),
  ];
}
