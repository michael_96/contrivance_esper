import 'package:esper/utils/app_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Home/home_tab.dart';
import '../Search/search_page.dart';
import '../Skill/skill_tab.dart';
import '../add_skill/add_skill.dart';
import '../../../model/bottom_bar_item.dart';


class AppBottomTabBar{

  List<BottomBarItem> tabItems =[
    BottomBarItem(tabWidget: const HomeTab(), tabBarItem: const BottomNavigationBarItem(
      icon: Icon(Icons.home_outlined),
      label: 'Home',
    )),
    BottomBarItem(tabWidget: SkillTab(), tabBarItem: const BottomNavigationBarItem(
      icon: Icon(Icons.star_border),
      label: 'Skill',
    )),
    BottomBarItem(tabWidget: const AddSkill(), tabBarItem: BottomNavigationBarItem(
      icon: Image.asset(EsparImageAssets.navPlus),
      // icon: Icon(Icons.add_circle_outline),
      label: '',
    ),),
    BottomBarItem(tabWidget: const SearchPage(), tabBarItem: BottomNavigationBarItem(
      icon: ImageIcon(
        AssetImage(EsparImageAssets.union),
      ),
      label: 'Community',
    )),
    BottomBarItem(tabWidget: const InDevelopment(), tabBarItem: const BottomNavigationBarItem(
      icon: Icon(Icons.notifications_none),
      label: 'Notification',
    ))

  ];



   Widget getWidgetAtIndex(int index) {
     return tabItems.elementAt(index).tabWidget;
   }
  List <BottomNavigationBarItem> getAllBottomNavigationItems() {
    List <BottomNavigationBarItem> bottomBarItems = [];
    for (var element in tabItems) {
      bottomBarItems.add(element.tabBarItem);
    }
    return bottomBarItems;
  }
}


class InDevelopment extends StatefulWidget {
  const InDevelopment({Key? key}) : super(key: key);

  @override
  State<InDevelopment> createState() => _InDevelopmentState();
}

class _InDevelopmentState extends State<InDevelopment> {
  @override
  Widget build(BuildContext context) {
    TextStyle optionStyle =
    const TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
    return  Center(
      child: Text(
        'Coming Soon',
        style: optionStyle,
      ),
    );
  }
}
