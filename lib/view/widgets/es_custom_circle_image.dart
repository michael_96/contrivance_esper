
import 'package:flutter/cupertino.dart';

class ESCustomCircleImage extends StatelessWidget {

  final String path;
  final double borderRadius;

  const ESCustomCircleImage({required this.path, required this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(borderRadius),
        child:  Image(
          image: AssetImage(path),
        ),
      ),
    );
  }
}