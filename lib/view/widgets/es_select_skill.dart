import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_images.dart';
import 'package:flutter/material.dart';
import '../../utils/app_text_styles.dart';


class ESSelectSkill extends StatefulWidget {
  final  skillData;
  ESSelectSkill(this.skillData);

  @override
  State<ESSelectSkill> createState() => _ESSelectSkillState();
}

class _ESSelectSkillState extends State<ESSelectSkill> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 20,
              bottom: 20,
            ),
            child: Text(
              'ADD STYLE',
              style: ESTextStyle.bodyHighlightBig ,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              bottom: 40.0,
            ),
            child: Text(
              widget.skillData.skillName,
              style: ESTextStyle.smallText1,
            ),
          ),
          SizedBox(height: 180, child: Image.asset(widget.skillData.skillImage)),
          Container(
            padding: const EdgeInsets.all(20),
            color: Colors.transparent,
            child: Card(
              elevation: 0,
              color: EsparColors.appTabThemeLight,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                            onPressed: () {
                              var type = widget.skillData.skillType;
                              print('Add button tapped for $type');
                            },
                            child: Image.asset(EsparImageAssets.roundAdd)),
                        Text( widget.skillData.skillType,
                            style: ESTextStyle.heading1)
                      ],
                    ),
                    Text(
                        widget.skillData.skillDescription,
                        style: ESTextStyle.mediumCopy),
                    const SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
