import 'package:esper/utils/app_text_styles.dart';
import 'package:flutter/material.dart';

class ESCustomCheckBox extends StatelessWidget {
  final bool checkboxOne;
  final String inputText;
  final double font_size;
  final Function(bool) onStateChanged;
  ESCustomCheckBox({required this.inputText,required this.font_size,required this.onStateChanged,
     required this.checkboxOne
  });


  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      tileColor: Colors.white70,
      activeColor: Colors.black54,
      checkColor: Colors.white,
      title: Text(
        inputText,
        style: ESTextStyle.heading1.merge(TextStyle(
          fontSize: font_size,
        ),),
      ),
      value: checkboxOne,
      onChanged: (newValue) {
        onStateChanged(checkboxOne);
        // setState(() {
        //   checkboxOne = !checkboxOne;
        // });
      },
      controlAffinity:
      ListTileControlAffinity.leading, //  <-- leading Checkbox
    );
  }
}