import 'package:esper/model/skill_criterion_data.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:esper/view/widgets/es_custom_checkbox.dart';
import 'package:esper/view/widgets/es_full_width_button.dart';
import 'package:flutter/material.dart';

class ESSkillCriterionBody extends StatefulWidget {
  final List<CriterionData> criterionList;
  const ESSkillCriterionBody(this.criterionList);

  @override
  State<ESSkillCriterionBody> createState() => _ESSkillCriterionBodyState();
}

class _ESSkillCriterionBodyState extends State<ESSkillCriterionBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          height: 60,
          color: Colors.white70,
          padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
          child: Text(
            'Qualifying Criteria',
            style: ESTextStyle.bodyHighlight,
          ),
        ),
        for (CriterionData item in widget.criterionList)
          ESCustomCheckBox(
              inputText: item.criterionData,
              font_size: 12,
              onStateChanged: (bool val) {
                setState(() {
                  item.criterionState = !item.criterionState;
                });
              },
              checkboxOne: item.criterionState),
        Container(
          color: Colors.white70,
          height: 40,
        ),
        ESFullWidthButton(
            inputText: 'SAVE',
            height: 60,
            onClickChanged: () {
              for (CriterionData item in widget.criterionList) {
                var data = item.criterionData;
                var state = item.criterionState;
                print('state of $data is $state');
              }
            })
      ],
    );
  }
}