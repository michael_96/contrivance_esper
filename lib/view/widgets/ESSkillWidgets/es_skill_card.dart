import 'package:esper/utils/app_colors.dart';
import 'package:flutter/material.dart';

class SkillCardItem {
  Widget headWidget;
  Widget changingWidget;
  Widget? tailWidget;
  bool swState;

  SkillCardItem(
      {required this.headWidget,
        required this.changingWidget,
        required this.swState,
        this.tailWidget});
}

class ESSkillCard extends StatefulWidget {
  final SkillCardItem _data;
  ESSkillCard(this._data);

  @override
  State<ESSkillCard> createState() => _ESSkillCardState();
}

class _ESSkillCardState extends State<ESSkillCard> {
  var bottomLeft = 8.0;
  var bottomRight = 8.0;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Colors.transparent,
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(8),
              topRight: const Radius.circular(8),
              bottomLeft: Radius.circular(bottomLeft),
              bottomRight: Radius.circular(bottomRight),
            ),
            child: Container(
              color: EsparColors.appContainer,
              child: TextButton(
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    alignment: Alignment.center),
                onPressed: () {
                  setState(() {
                    if (widget._data.swState) {
                      widget._data.swState = false;
                       bottomRight = 8;
                       bottomLeft = 8;
                    } else {
                      widget._data.swState = true;
                      bottomRight = 0;
                      bottomLeft = 0;
                    }
                  });
                },
                child: widget._data.headWidget,
              ),
            ),
          ),
          Visibility(
            visible: widget._data.swState,
            child: SizedBox(
              child: widget._data.changingWidget,
            ),
          ),
          SizedBox(
            child: widget._data.tailWidget,
          ),
        ],
      ),
    );
  }
}
