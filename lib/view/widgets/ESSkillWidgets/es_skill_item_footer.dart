
import 'package:esper/model/skill_footer_data.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:flutter/material.dart';

import '../es_custom_circle_image.dart';

class ESSkillItemFooter extends StatelessWidget {

  final SkillFooterData data;
  const ESSkillItemFooter(this.data);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const SizedBox(width: 50),
          for(String item in data.imageList)
            ESCustomCircleImage(borderRadius: 60,path: item),
           Text(
            data.additionalCount,
            style: ESTextStyle.smallText1,
          ),
          const SizedBox(width: 50),
        ],
      ),
    );
  }
}