import 'dart:ui';

import 'package:esper/model/skill_header_data.dart';
import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:flutter/material.dart';

class ESSkillItemHeader extends StatelessWidget {
  ESSkillItemHeader(this.skillData);
  final SkillHeaderData skillData;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child: Text(
                    skillData.skillName,
                    style: ESTextStyle.smallTextBold.merge(TextStyle(fontSize: 20)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 10, 0, 0),
                  child: Text(
                    skillData.skillLevel,
                    style: ESTextStyle.smallTextBold,
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: EsparColors.primaryHappyYellow,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: SizedBox(
                    height: 50,
                    width: 80,
                    child: Image.asset(EsparImageAssets.trophy),
                  ),
                ),
                Text(skillData.skillStatus,
                    style: ESTextStyle.bodyHighlight)
              ],
            ),
          ),
        ],
      ),
    );
  }
}