import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:flutter/material.dart';

class ESCustomButton extends StatelessWidget {
  final String inputText;
  final double width;
  final Function() onClickChanged;

   ESCustomButton({required this.inputText, required this.width, required this.onClickChanged});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: TextButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(EsparColors.lightButtonWhite),
            foregroundColor: MaterialStateProperty.all(EsparColors.textPrimary)),
        onPressed: () {
          onClickChanged();
        },
        child: Text(inputText ,style: ESTextStyle.smallTextBold,),
      ),
    );
  }
}