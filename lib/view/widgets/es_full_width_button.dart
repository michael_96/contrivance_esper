import 'package:esper/utils/app_text_styles.dart';
import 'package:flutter/material.dart';

class ESFullWidthButton extends StatelessWidget {
  final String inputText;
  final double height;
  final Function() onClickChanged;
  const ESFullWidthButton(
      {required this.inputText,
      required this.height,
      required this.onClickChanged});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: Colors.white,
      child: TextButton(
        style: TextButton.styleFrom(
            padding: EdgeInsets.zero,
            alignment: Alignment.center,
            backgroundColor: Colors.black54,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            primary: Colors.white),
        onPressed: () {
          onClickChanged();
        },
        child: Text(
          inputText,
          style: ESTextStyle.heading1.merge(TextStyle(color: Colors.white)),
        ),
      ),
    );
  }
}
