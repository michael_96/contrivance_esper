
class EsparImageAssets{
  static String addBg = 'images/addBg.png';
  static String addSkillImage = 'images/addSkillImage.png';
  static String avatar = 'images/avatar.png';
  static String bgSkill = 'images/bgSkill.png';
  static String dance = 'images/dance.png';
  static String dance2 = 'images/dance2.png';
  static String dance3 = 'images/dance3.png';
  static String dance4 = 'images/dance4.png';
  static String ic_white_add = 'images/ic_white_add.png';
  static String navPlus = 'images/navPlus.png';
  static String pr1 = 'images/pr1.png';
  static String pr2 = 'images/pr2.png';
  static String pr3 = 'images/pr3.png';
  static String roundAdd = 'images/roundAdd.png';
  static String searchBg = 'images/searchBg.png';
  static String trophy = 'images/trophy.png';
  static String union = 'images/union.png';
}

class EsparMenuAssets{
  static String ic_achievements = 'images/menu_images/ic_achievements.png';
  static String ic_activity = 'images/menu_images/ic_activity log.png';
  static String ic_chat = 'images/menu_images/ic_chat.png';
  static String ic_club = 'images/menu_images/ic_club.png';
  static String ic_cohort = 'images/menu_images/ic_cohort.png';
  static String ic_endorsement = 'images/menu_images/ic_endorsement.png';
  static String ic_invite = 'images/menu_images/ic_invite.png';
  static String ic_logout = 'images/menu_images/ic_logout.png';
  static String ic_msg = 'images/menu_images/ic_msg.png';
  static String ic_plus = 'images/menu_images/ic_plus.png';
  static String ic_profile = 'images/menu_images/ic_profile.png';
  static String ic_refer = 'images/menu_images/ic_refer.png';
  static String ic_settings = 'images/menu_images/ic_settings.png';
  static String ic_sig = 'images/menu_images/ic_sig.png';
  static String ic_skill = 'images/menu_images/ic_skills.png';
}