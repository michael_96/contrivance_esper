import 'dart:ui';

class EsparColors{
  static Color textPrimary = Color.fromRGBO(89, 88, 85, 1);
  static Color textSecondary = Color.fromRGBO(178, 166, 134, 1);
  static Color primaryHappyYellow = Color.fromRGBO(255, 210, 93, 1);
  static Color appTabTheme = Color.fromRGBO(242, 239, 230, 1);
  static Color appTabThemeDark = Color.fromRGBO(235, 195, 80, 1);
  static Color transparentThemeDark = Color.fromRGBO(89, 88, 85, 0.5);
  static Color appContainer = Color.fromRGBO(255, 219, 128, 1);
  static Color transparent = Color.fromRGBO(0, 0, 0, 0);
  static Color appTabThemeLight = Color.fromRGBO(242, 239, 230, 0.55);
  static Color lightButtonWhite = Color.fromRGBO(250, 249, 247, 0.85);




}