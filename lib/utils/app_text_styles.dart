import 'package:esper/utils/app_colors.dart';
import 'package:flutter/material.dart';

class ESTextStyle {

  static TextStyle bodyHighlightBig = TextStyle(
    color: EsparColors.textPrimary,
    fontFamily: 'OpenSans',
    fontSize: 22,
    fontWeight: FontWeight.bold,
  );
   static TextStyle bodyHighlight = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 12,
     fontWeight: FontWeight.bold,
   );
   static TextStyle heading2 = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 14,
     fontWeight: FontWeight.normal,
   );
   static TextStyle heading1 = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 16,
     fontWeight: FontWeight.bold,
   );
   static TextStyle smallText1 = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 10,
     fontWeight: FontWeight.normal,
   );
   static TextStyle smallestText = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 9,
     fontWeight: FontWeight.normal,
   );
   static TextStyle smallTextBold = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 10,
     fontWeight: FontWeight.bold,
   );
   static TextStyle mediumCopy = TextStyle(
     color: EsparColors.textPrimary,
     fontFamily: 'OpenSans',
     fontSize: 12,
     fontWeight: FontWeight.normal,
   );
}