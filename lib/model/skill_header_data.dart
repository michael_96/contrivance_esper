class SkillHeaderData {
  String skillName;
  String skillLevel;
  String skillStatus;
  SkillHeaderData(
      {required this.skillLevel,
        required this.skillName,
        required this.skillStatus});
}