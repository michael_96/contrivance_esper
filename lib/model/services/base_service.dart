abstract class BaseService {
  final String mediaBaseUrl = "https://itunes.apple.com/view.screens.search?term=";

  Future<dynamic> getResponse(String url);

}