import 'package:esper/model/skill_data.dart';
import 'package:esper/utils/app_images.dart';

class SelectListData {
  List<SelectSkillData> skillList = [
    SelectSkillData(skillName: 'BHARATNATYAM',
        skillImage: EsparImageAssets.dance,
        skillType: 'Kalakshetra',
        skillDescription: 'Integer vestibulum fringilla id lobortis ut. Nibh ullamcorper viverra lectus quis feugiat semper. Velit faucibus sed placerat praesent risus praesent donec. Faucibus viverra elementum tempus, magna mi faucibus est neque. Congue amet, vitae pulvinar dolor congue amet nulla. Dignissim tristique pellentesque quis porta eget. In a tristique quis nulla egestas non felis eget. '
    ),
    SelectSkillData(skillName: 'BHARATNATYAM',
        skillImage: EsparImageAssets.dance2,
        skillType: 'Pandanallur',
        skillDescription: 'Integer vestibulum fringilla id lobortis ut. Nibh ullamcorper viverra lectus quis feugiat semper. Velit faucibus sed placerat praesent risus praesent donec. Faucibus viverra elementum tempus, magna mi faucibus est neque. Congue amet, vitae pulvinar dolor congue amet nulla. Dignissim tristique pellentesque quis porta eget. In a tristique quis nulla egestas non felis eget. '
    ),
    SelectSkillData(skillName: 'BHARATNATYAM',
        skillImage: EsparImageAssets.dance3,
        skillType: 'Melattur',
        skillDescription: 'Integer vestibulum fringilla id lobortis ut. Nibh ullamcorper viverra lectus quis feugiat semper. Velit faucibus sed placerat praesent risus praesent donec. Faucibus viverra elementum tempus, magna mi faucibus est neque. Congue amet, vitae pulvinar dolor congue amet nulla. Dignissim tristique pellentesque quis porta eget. In a tristique quis nulla egestas non felis eget. '
    ),
    SelectSkillData(skillName: 'BHARATNATYAM',
        skillImage: EsparImageAssets.dance4,
        skillType: 'Kalaamandalam',
        skillDescription: 'Integer vestibulum fringilla id lobortis ut. Nibh ullamcorper viverra lectus quis feugiat semper. Velit faucibus sed placerat praesent risus praesent donec. Faucibus viverra elementum tempus, magna mi faucibus est neque. Congue amet, vitae pulvinar dolor congue amet nulla. Dignissim tristique pellentesque quis porta eget. In a tristique quis nulla egestas non felis eget. '
    ),
  ];
}