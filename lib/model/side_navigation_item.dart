import 'package:flutter/cupertino.dart';

class SideNavigationItem {
  String title;
  Widget leadingIconAsset;
  Widget? trailingIconAsset;
  bool isClickable;
  bool isHeader;

  SideNavigationItem({required this.title, required this.leadingIconAsset, this.trailingIconAsset, this.isClickable = true, this.isHeader = false});
}