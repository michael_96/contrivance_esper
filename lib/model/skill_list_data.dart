import 'package:esper/model/skill_footer_data.dart';
import 'package:esper/model/skill_header_data.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/view/widgets/ESSkillWidgets/es_skill_criterion_body.dart';
import 'package:esper/view/widgets/ESSkillWidgets/es_skill_card.dart';
import 'package:esper/view/widgets/ESSkillWidgets/es_skill_item_footer.dart';
import 'package:esper/view/widgets/ESSkillWidgets/es_skill_item_header.dart';
import 'skill_criterion_data.dart';

class SkillListData {
  final List<SkillCardItem> skillListData = [
    SkillCardItem(
      headWidget: ESSkillItemHeader(
        SkillHeaderData(
          skillLevel: "Level 1",
          skillName: "Basic Adavus and Mudras",
          skillStatus: "Virtuso",
        ),
      ),
      changingWidget: ESSkillCriterionBody([
        CriterionData(
            criterionData:
            'Criterion 1:  Congue amet, vitae pulvinar dolor congue amet'),
        CriterionData(
            criterionData:
            'Criterion 2:  Congue amet, vitae pulvinar dolor congue amet'),
        CriterionData(
            criterionData:
            'Criterion 3:  Congue amet, vitae pulvinar dolor congue amet'),
        CriterionData(
            criterionData:
            'Criterion 4:  Congue amet, vitae pulvinar dolor congue amet'),
      ]),
      swState: false,
      tailWidget: ESSkillItemFooter(SkillFooterData(imageList: [
        EsparImageAssets.pr1,
        EsparImageAssets.pr2,
        EsparImageAssets.pr3
      ], additionalCount: ' Other 10+')),
    ),
    SkillCardItem(
      headWidget: ESSkillItemHeader(
        SkillHeaderData(
          skillLevel: "Level 2",
          skillName: "Adavus and Body Movments",
          skillStatus: "Advance",
        ),
      ),
      changingWidget: ESSkillCriterionBody([
        CriterionData(
            criterionData:
            'Criterion 1:  Congue amet, vitae pulvinar dolor congue amet'),
        CriterionData(
            criterionData:
            'Criterion 2:  Congue amet, vitae pulvinar dolor congue amet'),
      ]),
      swState: false,
      tailWidget: ESSkillItemFooter(
        SkillFooterData(imageList: [
          EsparImageAssets.pr2,
          EsparImageAssets.pr1,
          EsparImageAssets.pr3
        ], additionalCount: ' Other 20+'),
      ),
    ),
  ];
}