class SelectSkillData {
  String skillName;
  String skillImage;
  String skillType;
  String skillDescription;

  SelectSkillData({required this.skillName, required this.skillImage, required this.skillType, required this.skillDescription});
}