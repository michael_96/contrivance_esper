
class SkillFooterData {
  List<String> imageList;
  String additionalCount;

  SkillFooterData({required this.imageList, required this.additionalCount});
}