import 'package:flutter/cupertino.dart';

class BottomBarItem {
  Widget tabWidget;
  BottomNavigationBarItem tabBarItem;

  BottomBarItem({required this.tabWidget, required this.tabBarItem});
}