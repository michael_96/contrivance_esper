import 'package:esper/utils/app_colors.dart';
import 'package:esper/utils/app_images.dart';
import 'package:esper/utils/app_text_styles.dart';
import 'package:esper/view/screens/bottom_navigation/app_bottom_tab_bar.dart';
import 'package:esper/view/screens/side_navigation/side_nav_bar.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(const MyStatefulWidget());
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    AppBottomTabBar bottomTabBar = AppBottomTabBar();
    SideNavBar sideNavBar = SideNavBar();
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset : false,
          body: SafeArea(
            child: bottomTabBar.getWidgetAtIndex(_selectedIndex),
          ),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: EsparColors.appTabTheme,
            type: BottomNavigationBarType.fixed,
            items: bottomTabBar.getAllBottomNavigationItems(),
            currentIndex: _selectedIndex,
            selectedItemColor: EsparColors.appTabThemeDark,
            onTap: _onItemTapped,
          ),
          drawer: Drawer(
            backgroundColor: EsparColors.appTabTheme,
            child: SafeArea(
              child: ListView.builder(
                itemExtent: 40.0,
                itemCount: sideNavBar.list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: sideNavBar.list[index].leadingIconAsset,
                    trailing: sideNavBar.list[index].trailingIconAsset,
                    title: Text(
                      sideNavBar.list[index].title,
                      style: ESTextStyle.heading2.merge(TextStyle(
                        fontWeight: sideNavBar.list[index].isHeader ? FontWeight.bold : FontWeight.normal ,
                      )) ,
                    ),
                    onTap: () {
                      setState(() {
                        if (sideNavBar.list[index].isClickable) {
                          Navigator.pop(context);
                          print('You just tapped ${sideNavBar.list[index].title}');
                        }
                      });
                    },
                  );
                },
              ),
            ),
          ),
          appBar: AppBar(
            backgroundColor: EsparColors.appTabTheme,
            foregroundColor: EsparColors.primaryHappyYellow,
            actions: [
              Image.asset(EsparImageAssets.avatar),
              const SizedBox(
                width: 15,
                height: 25,
              )
            ],
          ),
        ));
  }
}